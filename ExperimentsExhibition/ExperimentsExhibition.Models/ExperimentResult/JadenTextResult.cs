﻿using System;

namespace ExperimentsExhibition.Models.ExperimentResult
{
    public sealed class JadenTextResult
    {
        public int Duration { get; set; }
        
        public string Context { get; set; }

        public DateTime RunDate { get; set; }
    }
}