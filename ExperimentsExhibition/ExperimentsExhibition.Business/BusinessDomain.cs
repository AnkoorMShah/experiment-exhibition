﻿using ExperimentsExhibition.Business.Managers;
using ExperimentsExhibition.Business.Managers.ServiceLocators;
using ExperimentsExhibition.Business.Managers.ServiceLocators.DependencyInterfaces;
using ExperimentsExhibition.Models.ExperimentResult;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ExperimentsExhibition.Business
{
    public sealed class BusinessDomain : IDisposable
    {
        private bool _disposed;
        private readonly IMangerProvider _managerProvider;

        private ExperimentManager _defaultexperimentManager;
        private ExperimentManager DefaultExperimentManager
        {
            get
            {
                return _defaultexperimentManager ?? (_defaultexperimentManager = _managerProvider.CreateExperimentManager());
            }
        }

        public BusinessDomain() : this(new DefaultServiceLocator())
        {
        }

        internal BusinessDomain(IMangerProvider serviceLocator)
        {
            _managerProvider = serviceLocator;
        }

        public Task<IEnumerable<JadenTextResult>> GetJadenTextResults()
        {
            return DefaultExperimentManager.GetJadenTextResults();
        }

        private void Dispose(bool disposing)
        {
            if(disposing && !_disposed)
            {
                _defaultexperimentManager?.Dispose();
                _disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}