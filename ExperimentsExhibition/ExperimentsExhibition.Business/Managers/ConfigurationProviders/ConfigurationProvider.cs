﻿using Microsoft.Extensions.Configuration;

namespace ExperimentsExhibition.Business.Managers.ConfigurationProviders
{
    internal sealed class ConfigurationProvider : ConfigurationProviderBase
    {
        private readonly IConfigurationRoot _configurationRoot;

        public ConfigurationProvider()
        {
            ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
            configurationBuilder.AddJsonFile("appsettings.json");
            _configurationRoot = configurationBuilder.Build();
        }

        protected override string RetrieveConfigurationSettingValue(string key)
        {
            return _configurationRoot["AppSettings:" + key];
        }
    }
}