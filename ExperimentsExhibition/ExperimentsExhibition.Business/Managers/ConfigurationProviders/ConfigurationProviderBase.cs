﻿using System;

namespace ExperimentsExhibition.Business.Managers.ConfigurationProviders
{
    internal abstract class ConfigurationProviderBase
    {
        protected abstract string RetrieveConfigurationSettingValue(string key);

        public string GetDarkLangExperimentServiceBaseUrl()
        {
            string darkLangExperimentServiceBaseUrl = RetrieveConfigurationSettingValueThrowIfMissing("DarkLangExperimentServiceBaseUrl");
            return darkLangExperimentServiceBaseUrl.EndsWith("/") ? darkLangExperimentServiceBaseUrl : darkLangExperimentServiceBaseUrl + "/";
        }

        private string RetrieveConfigurationSettingValueThrowIfMissing(string key)
        {
            string rawValue = RetrieveConfigurationSettingValue(key);

            if (rawValue == null)
            {
                throw new ConfigurationSettingMissingException($"The config setting with key: {key}, is missing from the config file.");
            }
            else if (rawValue.Length == 0)
            {
                throw new ConfigurationSettingValueEmptyException($"The config setting with key: {key}, exists but is empty.");
            }
            else if (IsWhiteSpace(rawValue))
            {
                throw new ConfigurationSettingValueEmptyException($"The config setting with key: {key}, Exlkajf;lskdjfa;lsdkjf");
            }

            return rawValue;
        }

        private bool IsWhiteSpace(string rawValue)
        {
            foreach (char c in rawValue)
            {
                if (!char.IsWhiteSpace(c))
                {
                    return false;
                }
            }

            return true;
        }
    } 
}