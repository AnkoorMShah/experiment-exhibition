﻿using ExperimentsExhibition.Business.Managers.Exceptions;
using System;
using System.Runtime.Serialization;

namespace ExperimentsExhibition.Business.Managers.Exceptions
{
    [Serializable]
    public sealed class ConfigurationSettingMissingException : ExperimentsExhibitionTechnicalBaseException
    {
        public ConfigurationSettingMissingException()
        {
        }

        public ConfigurationSettingMissingException(string message) : base(message)
        {
        }

        public ConfigurationSettingMissingException(string message, Exception innerException) : base(message, innerException)
        {
        }

        private ConfigurationSettingMissingException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}