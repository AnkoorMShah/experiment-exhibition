﻿using System;

namespace ExperimentsExhibition.Business.Managers.Exceptions
{

    [Serializable]
    public abstract class ExperimentsExhibitionTechnicalBaseException : ExperimentsExhibitionBaseException
    {
        public ExperimentsExhibitionTechnicalBaseException() { }
        public ExperimentsExhibitionTechnicalBaseException(string message) : base(message) { }
        public ExperimentsExhibitionTechnicalBaseException(string message, System.Exception inner) : base(message, inner) { }
        protected ExperimentsExhibitionTechnicalBaseException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}