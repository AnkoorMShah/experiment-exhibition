﻿using ExperimentsExhibition.Business.Managers.Exceptions;
using System;
using System.Runtime.Serialization;

namespace ExperimentsExhibition.Business.Managers.Exceptions
{
    [Serializable]
    public sealed class InvalidJadenTextResultException : ExperimentsExhibitionBusinessBaseException
    {
        public InvalidJadenTextResultException()
        {
        }

        public InvalidJadenTextResultException(string message) : base(message)
        {
        }

        public InvalidJadenTextResultException(string message, Exception innerException) : base(message, innerException)
        {
        }

        private InvalidJadenTextResultException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}