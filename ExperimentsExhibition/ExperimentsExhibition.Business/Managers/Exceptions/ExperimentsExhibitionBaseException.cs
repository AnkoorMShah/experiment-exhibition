﻿using System;

namespace ExperimentsExhibition.Business.Managers.Exceptions
{

    [Serializable]
    public abstract class ExperimentsExhibitionBaseException : Exception
    {
        public ExperimentsExhibitionBaseException() { }
        public ExperimentsExhibitionBaseException(string message) : base(message) { }
        public ExperimentsExhibitionBaseException(string message, System.Exception inner) : base(message, inner) { }
        protected ExperimentsExhibitionBaseException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}