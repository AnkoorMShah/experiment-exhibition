﻿using System;

namespace ExperimentsExhibition.Business.Managers.Exceptions
{

    [Serializable]
    public abstract class ExperimentsExhibitionBusinessBaseException : Exception
    {
        public ExperimentsExhibitionBusinessBaseException() { }
        public ExperimentsExhibitionBusinessBaseException(string message) : base(message) { }
        public ExperimentsExhibitionBusinessBaseException(string message, Exception inner) : base(message, inner) { }
        protected ExperimentsExhibitionBusinessBaseException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}