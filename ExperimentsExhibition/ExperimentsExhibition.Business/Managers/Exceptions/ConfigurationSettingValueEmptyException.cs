﻿using ExperimentsExhibition.Business.Managers.Exceptions;
using System;
using System.Runtime.Serialization;

namespace ExperimentsExhibition.Business.Managers.Exceptions
{
    [Serializable]
    public sealed class ConfigurationSettingValueEmptyException : ExperimentsExhibitionTechnicalBaseException
    {
        public ConfigurationSettingValueEmptyException()
        {
        }

        public ConfigurationSettingValueEmptyException(string message) : base(message)
        {
        }

        public ConfigurationSettingValueEmptyException(string message, Exception innerException) : base(message, innerException)
        {
        }

        private ConfigurationSettingValueEmptyException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}