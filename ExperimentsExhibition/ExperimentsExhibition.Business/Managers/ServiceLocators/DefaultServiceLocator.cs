﻿using ExperimentsExhibition.Business.Managers.ConfigurationProviders;
using ExperimentsExhibition.Business.Managers.ServiceLocators.DependencyInterfaces;
using ExperimentsExhibition.Business.Managers.Services.DarkLangExperimentService;
using System.Net.Http;

namespace ExperimentsExhibition.Business.Managers.ServiceLocators
{
    internal sealed class DefaultServiceLocator : 
        ServiceLocatorBase, 
        IMangerProvider, 
        IConfigurationProviderProvider, 
        IDarkLangExperimentGatewayProvider,
        IHttpMessageHandlerProvider
    {
        public DarkLangExperimentGateway CreateDarkLangExperimentGateway(string serviceBaseUrl)
        {
            return new DarkLangExperimentGateway(this, serviceBaseUrl);
        }

        public ConfigurationProviderBase CreateDefaultConfigurationProvider()
        {
            return new ConfigurationProvider();
        }

        public ExperimentManager CreateExperimentManager()
        {
            return new ExperimentManager(this, this);
        }

        public HttpMessageHandler CreateHttpMessageHandler()
        {
            return new HttpClientHandler();
        }
    }
}