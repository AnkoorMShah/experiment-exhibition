﻿using ExperimentsExhibition.Business.Managers.ConfigurationProviders;

namespace ExperimentsExhibition.Business.Managers.ServiceLocators.DependencyInterfaces
{
    internal interface IConfigurationProviderProvider
    {
        ConfigurationProviderBase CreateDefaultConfigurationProvider();
    }
}