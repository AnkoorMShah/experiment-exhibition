﻿namespace ExperimentsExhibition.Business.Managers.ServiceLocators.DependencyInterfaces
{
    internal interface IMangerProvider
    {
        ExperimentManager CreateExperimentManager();
    }
}