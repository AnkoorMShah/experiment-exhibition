﻿using System.Net.Http;

namespace ExperimentsExhibition.Business.Managers.ServiceLocators.DependencyInterfaces
{
    internal interface IHttpMessageHandlerProvider
    {
        HttpMessageHandler CreateHttpMessageHandler();
    }
}