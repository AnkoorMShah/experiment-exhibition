﻿using ExperimentsExhibition.Business.Managers.Services.DarkLangExperimentService;

namespace ExperimentsExhibition.Business.Managers.ServiceLocators.DependencyInterfaces
{
    internal interface IDarkLangExperimentGatewayProvider
    {
        DarkLangExperimentGateway CreateDarkLangExperimentGateway(string serviceUrl);
    }
}