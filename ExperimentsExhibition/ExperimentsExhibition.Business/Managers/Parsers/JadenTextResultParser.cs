﻿using ExperimentsExhibition.Business.Managers.Exceptions;
using ExperimentsExhibition.Models.ExperimentResult;
using System;

namespace ExperimentsExhibition.Business.Managers.Parsers
{
    public static class JadenTextResultParser
    {
        //if there were a constant list of values for string to, say, enum, make a dictionary to map them, according to shiv
        //also would make a dictionary to map from enum to string, according to shiv
        //also make a variable for all valid strings, for exception message
        private static readonly string _formatDefinition = "Duration | Summary | Date";


        static JadenTextResultParser()
        {
            //FieldInfo[] fieldInfos = typeof(Genre).GetFields(BindingFlags.Public | BindingFlags.Static);
            //populate the dictionaries from before
        }

        public static JadenTextResult Parse(string jadenTextResultAsString)
        {
            if(string.IsNullOrEmpty(jadenTextResultAsString))
            {
                throw new InvalidJadenTextResultException($"The string cannot be null or empty. Valid values are fomatted as: {_formatDefinition}");
            }

            string[] jadenTextResultComponents = jadenTextResultAsString.Split(" | ");

            if(jadenTextResultComponents.Length == 3)
            {
                throw new InvalidJadenTextResultException($"The string: {jadenTextResultAsString}, has the incorrect number of components. Valid values are fomatted as: {_formatDefinition}");
            }

            return new JadenTextResult()
            {
                Duration = int.Parse(jadenTextResultComponents[0]),
                Context = jadenTextResultComponents[1],
                RunDate = DateTime.Parse(jadenTextResultComponents[2])
            };
        }

        public static string ToString(JadenTextResult jadenTextResult)
        {
            Validate(jadenTextResult);
            return $"{jadenTextResult.Duration} | {jadenTextResult.Context} | {jadenTextResult.RunDate}";
        }

        public static void Validate(JadenTextResult jadenTextResult)
        {
            if(jadenTextResult.Duration<=0 ||
               string.IsNullOrEmpty(jadenTextResult.Context) || 
               jadenTextResult.RunDate == null)
            {
                throw new InvalidJadenTextResultException($"The stored data is invalid as per business requirements. The duration must be postivie, the Rundate and Context must be provided, not empty/null");
            }
        }
    }
}
