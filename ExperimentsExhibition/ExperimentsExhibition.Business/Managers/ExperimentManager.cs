﻿using ExperimentsExhibition.Business.Managers.ConfigurationProviders;
using ExperimentsExhibition.Business.Managers.DataLayer;
using ExperimentsExhibition.Business.Managers.ServiceLocators.DependencyInterfaces;
using ExperimentsExhibition.Business.Managers.Services.DarkLangExperimentService;
using ExperimentsExhibition.Models.ExperimentResult;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ExperimentsExhibition.Business.Managers
{
    internal sealed class ExperimentManager : IDisposable
    {
        private bool _disposed;

        private IConfigurationProviderProvider _configurationProviderProvider;
        private IDarkLangExperimentGatewayProvider _darkLangExperimentGatewayProvider;

        private ConfigurationProviderBase _configurationProvider;
        private ConfigurationProviderBase ConfigurationProvider
        {
            get
            {
                return _configurationProvider ?? (_configurationProvider = _configurationProviderProvider.CreateDefaultConfigurationProvider());
            }
        }

        private DarkLangExperimentGateway _experimentGateway;
        private DarkLangExperimentGateway ExperimentGateway
        {
            get
            {
                return _experimentGateway ?? (
                    _experimentGateway = _darkLangExperimentGatewayProvider.CreateDarkLangExperimentGateway(
                        ConfigurationProvider.GetDarkLangExperimentServiceBaseUrl()
                    ));
            }
        }

        private DataFacade _dataFacade;

        private DataFacade DataFacade
        {
            get
            {
                return _dataFacade ?? (_dataFacade = new DataFacade());
            }
        }

        public ExperimentManager(
            IConfigurationProviderProvider configurationProviderProvider, 
            IDarkLangExperimentGatewayProvider darkLangExperimentGatewayProvider)
        {
            this._configurationProviderProvider = configurationProviderProvider;
            this._darkLangExperimentGatewayProvider = darkLangExperimentGatewayProvider;
        }

        public async Task<IEnumerable<JadenTextResult>> GetJadenTextResults()
        {
            //TODO: run simulations
            //TODO: cache results into db via datafacade.
            //TODO: add results to db

            //post current results to DarkLang, and get all archived results.
            IEnumerable<JadenTextResult> archivedJadenTextResults = await ExperimentGateway.GetArchivedJadenTextResults();

            //TODO: add missing results to local db
                //TODO: add only missing queries to darklang db

            return archivedJadenTextResults;
        }

        public Task<IEnumerable<JadenTextResult>> GetAllJadenTextResults()
        {
            //TODO: retrieve from datafacade, and combine results from service.
            throw new NotImplementedException();
        }

        private void Dispose(bool disposing)
        {
            if (disposing && !_disposed)
            {
                _experimentGateway?.Dispose();
                _disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}