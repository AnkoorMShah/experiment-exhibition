﻿using ExperimentsExhibition.Business.Managers.ServiceLocators.DependencyInterfaces;
using ExperimentsExhibition.Business.Managers.Services.DarkLangExperimentService.Exceptions;
using ExperimentsExhibition.Business.Managers.Services.DarkLangExperimentService.ResourceModels;
using ExperimentsExhibition.Models.ExperimentResult;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace ExperimentsExhibition.Business.Managers.Services.DarkLangExperimentService
{
    internal sealed class DarkLangExperimentGateway : IDisposable
    {
        private bool _disposed;
        private HttpClient _httpClient;
        private readonly IHttpMessageHandlerProvider _httpMessageHandlerProvider;

        private static readonly string _durationEndpoint = "demo/GetJadenDurations";
        private static readonly string _dateEndpoint = "demo/GetJadenDates";
        private static readonly string _summaryEndpoint = "demo/GetJadenSummaries";

        public DarkLangExperimentGateway(IHttpMessageHandlerProvider httpMessageHandlerProvider, string baseUrl)
        {
            _httpMessageHandlerProvider = httpMessageHandlerProvider;
            _httpClient = CreateHttpClient(baseUrl);
        }

        private HttpClient CreateHttpClient(string baseUrl)
        {
            HttpClient httpClient = new HttpClient(_httpMessageHandlerProvider.CreateHttpMessageHandler());
            httpClient.BaseAddress = new Uri(baseUrl);
            return httpClient;
        }

        public async Task<IEnumerable<JadenTextResult>> GetArchivedJadenTextResults()
        {
            //TODO: darklang db multiple tables and requests that map ID-one of the relavent fields
            //TODO: Test if this works lol

            Task<HttpResponseMessage> httpResponseMessageTaskForDurations = _httpClient.GetAsync(_durationEndpoint); //TODO: these endpoints in darklang
            Task<HttpResponseMessage> httpResponseMessageTaskForDates = _httpClient.GetAsync(_dateEndpoint);
            Task<HttpResponseMessage> httpResponseMessageTaskForSummaries = _httpClient.GetAsync(_summaryEndpoint);

            await Task.WhenAll(httpResponseMessageTaskForDurations, httpResponseMessageTaskForDates, httpResponseMessageTaskForSummaries);

            HttpResponseMessage httpResponseMessageForDurations = httpResponseMessageTaskForDurations.Result;
            HttpResponseMessage httpResponseMessageForDates = httpResponseMessageTaskForDates.Result;
            HttpResponseMessage httpResponseMessageForSummaries = httpResponseMessageTaskForSummaries.Result;

            Task<IEnumerable<JadenTextResultResource>> jadenTextResultResourcesTaskWithDuration = httpResponseMessageForDurations
                .Content
                .ReadAsAsync<IEnumerable<JadenTextResultResource>>();
            Task<IEnumerable<JadenTextResultResource>> jadenTextResultResourcesTaskWithDates = httpResponseMessageForDates
                .Content
                .ReadAsAsync<IEnumerable<JadenTextResultResource>>();
            Task<IEnumerable<JadenTextResultResource>> jadenTextResultResourcesTaskWithSummaries = httpResponseMessageForSummaries
                .Content
                .ReadAsAsync<IEnumerable<JadenTextResultResource>>();

            await Task.WhenAll(
                jadenTextResultResourcesTaskWithDuration, 
                jadenTextResultResourcesTaskWithDates, 
                jadenTextResultResourcesTaskWithSummaries);

            IEnumerable<JadenTextResultResource> jadenTextResultResourcesWithDurations = 
                jadenTextResultResourcesTaskWithDuration
                .Result
                .OrderBy(m => m.ID);
            IEnumerable<JadenTextResultResource> jadenTextResultResourcesWithDates = 
                jadenTextResultResourcesTaskWithDates
                .Result
                .OrderBy(m => m.ID);
            IEnumerable<JadenTextResultResource> jadenTextResultResourcesWithSummaries = 
                jadenTextResultResourcesTaskWithSummaries
                .Result
                .OrderBy(m => m.ID);

            List<JadenTextResult> jadenTextResults = new List<JadenTextResult>();

            using(IEnumerator<JadenTextResultResource> jadenTextResultResourcesWithDurationsEnumerator = 
                jadenTextResultResourcesWithDurations.GetEnumerator())
            using (IEnumerator<JadenTextResultResource> jadenTextResultResourcesWithDatesEnumerator =
                jadenTextResultResourcesWithDates.GetEnumerator())
            using (IEnumerator<JadenTextResultResource> jadenTextResultResourcesWithSummariesEnumerator =
                jadenTextResultResourcesWithSummaries.GetEnumerator())
            {
                while(jadenTextResultResourcesWithDurationsEnumerator.MoveNext())
                {
                    JadenTextResultResource jadenTextResultResourceWithDuration = jadenTextResultResourcesWithDurationsEnumerator.Current;

                    jadenTextResultResourcesWithDatesEnumerator.MoveNext();
                    JadenTextResultResource jadenTextResultResourceWithDate = jadenTextResultResourcesWithDatesEnumerator.Current;

                    jadenTextResultResourcesWithSummariesEnumerator.MoveNext();
                    JadenTextResultResource jadenTextResultResourceWithSummary = jadenTextResultResourcesWithSummariesEnumerator.Current;

                    jadenTextResults.Add(new JadenTextResult
                    {
                        Duration = jadenTextResultResourceWithDuration.Duration,
                        Context = jadenTextResultResourceWithSummary.Summary,
                        RunDate = DateTime.Parse(jadenTextResultResourceWithDate.RunDate)
                    });
                }
            }


            return jadenTextResults;
        }

        private static void EnsureSuccess(bool isSuccessStatusCode, HttpStatusCode statusCode)
        {
            if(!isSuccessStatusCode)
            {
                ThrowOnServiceNotFound(statusCode);
            }
        }

        private static void ThrowOnServiceNotFound(HttpStatusCode statusCode)
        {
            switch(statusCode)
            {
                case HttpStatusCode.BadGateway:
                case HttpStatusCode.BadRequest:
                    break;
                case HttpStatusCode.NotFound:
                    throw new DarkLangExperimentServiceNotFoundException("Service call resulted in a Not Found status code");
                default:
                    break;
            }
        }

        private void Dispose(bool disposing)
        {
            if(disposing && !_disposed)
            {
                _httpClient?.Dispose();
                _disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}