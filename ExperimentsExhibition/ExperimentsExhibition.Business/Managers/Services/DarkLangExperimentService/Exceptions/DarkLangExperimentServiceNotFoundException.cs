﻿using ExperimentsExhibition.Business.Managers.Exceptions;
using System;
using System.Runtime.Serialization;

namespace ExperimentsExhibition.Business.Managers.Services.DarkLangExperimentService.Exceptions
{
    [Serializable]
    public class DarkLangExperimentServiceNotFoundException : ExperimentsExhibitionTechnicalBaseException
    {
        public DarkLangExperimentServiceNotFoundException()
        {
        }

        public DarkLangExperimentServiceNotFoundException(string message) : base(message)
        {
        }

        public DarkLangExperimentServiceNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        private DarkLangExperimentServiceNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}