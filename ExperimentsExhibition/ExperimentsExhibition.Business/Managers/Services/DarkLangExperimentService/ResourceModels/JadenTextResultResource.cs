﻿namespace ExperimentsExhibition.Business.Managers.Services.DarkLangExperimentService.ResourceModels
{
    internal sealed class JadenTextResultResource
    {
        public int ID;
        public int Duration { get; set; }
        public string Summary { get; set; }
        public string RunDate { get; set; }
    }
}