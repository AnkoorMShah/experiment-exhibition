﻿using ExperimentsExhibition.Models.ExperimentResult;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ExperimentsExhibition.Business.Managers.DataLayer
{
    internal sealed class DataFacade
    {
        private JadenTextResultDataManager _jadenTextResultManager;
        private JadenTextResultDataManager JadenTextResultDataManager
        {
            get
            {
                return _jadenTextResultManager ?? (_jadenTextResultManager = new JadenTextResultDataManager());
            }
        }

        public async Task CreateJadenTextResult(JadenTextResult jadenTextResult)
        {
            await JadenTextResultDataManager.CreateJadenTextResult(jadenTextResult);
        }

        public Task<IEnumerable<JadenTextResult>> GetAllJadenTextResults()
        {
            return JadenTextResultDataManager.GetAllMovies();
        }
    }
}