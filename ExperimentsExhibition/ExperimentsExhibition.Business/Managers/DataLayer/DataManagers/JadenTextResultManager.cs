﻿using ExperimentsExhibition.Models.ExperimentResult;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace ExperimentsExhibition.Business
{
    internal sealed class JadenTextResultDataManager
    {
        private readonly SqlClientFactory sqlClientFactory = SqlClientFactory.Instance;

        public async Task CreateJadenTextResult(JadenTextResult jadenTextResult)
        {
            DbConnection dbConnection = CreateDbConnection();
            DbCommand dbCommand = null;
            DbTransaction dbTransaction = null;

            try
            {
                await dbConnection.OpenAsync();
                dbTransaction = dbConnection.BeginTransaction();
                dbCommand = dbConnection.CreateCommand();
                dbCommand.Transaction = dbTransaction;

                dbCommand.CommandType = CommandType.StoredProcedure;
                dbCommand.CommandText = "dbo.__________"; //TODO: make db in vs :V


                AddDbParameter(dbCommand, "@Duration", jadenTextResult.Duration, DbType.Int32, 0);
                AddDbParameter(dbCommand, "@Summary", jadenTextResult.Duration, DbType.String, 50);
                AddDbParameter(dbCommand, "@DateTime", jadenTextResult.Duration, DbType.String, 50);

                await dbCommand.ExecuteNonQueryAsync();
                dbTransaction.Commit();
            }
            catch(DbException)
            {
                dbTransaction?.Rollback();
                throw;
            }
            finally
            {
                dbCommand?.Dispose();
                dbTransaction?.Dispose();
                if(dbConnection.State == ConnectionState.Open)
                {
                    dbConnection.Close();
                }
            }
        }

        private void AddDbParameter(DbCommand dbCommand, string parameterName, object value, DbType dbType, int size)
        {
            DbParameter dbParameter = dbCommand.CreateParameter();
            dbParameter.ParameterName = parameterName;
            dbParameter.Value = value;
            dbParameter.DbType = dbType;
            dbParameter.Size = size;
            dbCommand.Parameters.Add(dbParameter);
        }

        public async Task<IEnumerable<JadenTextResult>> GetAllMovies()
        {
            DbConnection dbConnection = CreateDbConnection();
            DbCommand dbCommand = null;

            try
            {
                await dbConnection.OpenAsync();
                dbCommand = dbConnection.CreateCommand();
                dbCommand.CommandType = CommandType.StoredProcedure;
                dbCommand.CommandText = "dbo.____________"; //TODO: help iunno how to us vs to make db
                DbDataReader dbDataReader = await dbCommand.ExecuteReaderAsync();
                return MapToJadenTextResult(dbDataReader);
            }
            finally
            {
                dbCommand?.Dispose();
                if(dbConnection.State == ConnectionState.Open)
                {
                    dbConnection.Close();
                }
            }
        }

        private IEnumerable<JadenTextResult> MapToJadenTextResult(DbDataReader dbDataReader)
        {
            List<JadenTextResult> jadenTextResults = new List<JadenTextResult>();

            while(dbDataReader.Read())
            {
                jadenTextResults.Add(new JadenTextResult()
                {
                    Duration = (int)dbDataReader[0],
                    Context = (string)dbDataReader[1],
                    RunDate = DateTime.Parse((string)dbDataReader[2])
                });
            }

            return jadenTextResults;
        }

        private DbConnection CreateDbConnection()
        {
            DbConnection dbConnection = sqlClientFactory.CreateConnection();
            dbConnection.ConnectionString = @"Data Source=(localdb)\ProjectsV13;Initial Catalog=JadenTextResultDb;Integrated Security=True;TrustServerCertificate=True;";
            return dbConnection;
        }
    }
}