﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ExperimentsExhibition.Business;
using ExperimentsExhibition.ModelMappers;
using ExperimentsExhibition.Models.ExperimentResult;
using ExperimentsExhibition.ResourceModels;
using Microsoft.AspNetCore.Mvc;

namespace ExperimentsExhibition.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ExperimentSelectionController : ControllerBase
    {
        [HttpGet]
        public async Task<IEnumerable<JadenTextResultResource>> Get()
        {
            IEnumerable<JadenTextResult> jadenTextResults = await new BusinessDomain().GetJadenTextResults();
            return jadenTextResults.Select(jTR => ResourceModelMapper.ToJadenTextResultResource(jTR)).AsEnumerable();
        }
    }    
}