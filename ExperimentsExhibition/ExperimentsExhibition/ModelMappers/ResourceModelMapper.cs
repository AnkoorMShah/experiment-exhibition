﻿using ExperimentsExhibition.Models.ExperimentResult;
using ExperimentsExhibition.ResourceModels;
using System;

namespace ExperimentsExhibition.ModelMappers
{
    internal sealed class ResourceModelMapper
    {
        internal static JadenTextResultResource ToJadenTextResultResource(JadenTextResult jtResult)
        {
            return new JadenTextResultResource
            {
                Duration = jtResult.Duration
            };
        }
    }
}